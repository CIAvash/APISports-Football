use Cro::HTTP::Router:ver<0.8.*>;

sub routes is export {
    route {
        get -> $route, :%params {
            my Str:D $params is default('') = %params.sort.map({ "-{.key}({.value})" }).join if %params;

            content 'application/json', $?FILE.IO.parent.add('data', "$route$params.json").slurp
        }
    }
}
