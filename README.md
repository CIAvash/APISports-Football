NAME
====

APISports::Football - An interface to [API-Football](https://www.api-football.com/).

DESCRIPTION
===========

APISports::Football is a [Raku](https://www.raku-lang.ir/en) module for interfacing with [API Sports Football](https://v3.football.api-sports.io).

An API key is required to use the API.

SYNOPSIS
========

```raku
use APISports::Football:auth<zef:CIAvash>;

my $f = APISports::Football.new: :api_key<1234>;

my @competitions = $f.competitions: name => 'premier league', :current, type => LeagueType::League, :country<england>;
```

INSTALLATION
============

You need to have [Raku](https://www.raku-lang.ir/en) and [zef](https://github.com/ugexe/zef), then run:

```console
zef install "APISports::Football:auth<zef:CIAvash>"
```

or if you have cloned the repo:

```console
zef install .
```

TESTING
=======

```console
prove -ve 'raku -I.' --ext rakutest
```

or

```console
prove6 -I. -v
```

ATTRIBUTES
==========

## has Str $.api_key

API key required by api-football.com

## has APISports::Football::HTTPClient $.http_client

An object for making requests to api-football.com

METHODS
=======

## method timezones

```raku
method timezones(
    Bool :h(:$http_body)
) returns Mu
```

Get the respone object containing list of available timezones.

Bool :h(:$http_body)
----------

Get the HTTP response body

## method countries

```raku
method countries(
    Bool :h(:$http_body),
    *%params (Str :$name, Str :$code where { ... }, Str :$search where { ... })
) returns Mu
```

Get the respone object containing list of available countries.

### Bool :h(:$http_body)

Get the HTTP response body

### Str :$name

Name of the country

### TwoChars :$code

Code of the country

### SearchTerm :$search

Name of the country

## method seasons

```raku
method seasons(
    Bool :h(:$http_body)
) returns Mu
```

Get the respone object containing list of available seasons for competitions.

### Bool :h(:$http_body)

Get the HTTP response body

## method competitions

```raku
method competitions(
    Bool :h(:$http_body),
    *%params (Int :$id where { ... }, Str :$name, Str :$country, Str :$code where { ... }, Int :$season where { ... }, Int :$team where { ... }, LeagueType(Str) :$type, Bool :$current, Str :$search where { ... }, Int :$last where { ... })
) returns Mu
```

Get the respone object containing list of available competitions.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$id

ID of the league

### Str :$name

Name of the league

### Str :$country

Country name of the league

### TwoChars :$code

Code of the country

### Year :$season

Season of the league

### ID :$team

ID of the team

### LeagueType(Str) :$type

Type of the league

### Bool :$current

State of the league

### SearchTerm :$search

Name or country name of the league

### AtMost2Digits :$last

The X last leagues added to the API

## method clubs

```raku
method clubs(
    Bool :h(:$http_body),
    *%params (Int :$id where { ... }, Str :$name, Int :$league where { ... }, Str :$country, Int :$season where { ... }, Str :$search where { ... })
) returns Mu
```

Get the respone object containing list of available clubs.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$id

ID of the team

### Str :$name

Name of the team

### ID :$league

ID of the league

### Str :$country

Country name of the team

### Year :$season

Season of the league

### SearchTerm :$search

Name or country name of the team

## method team_stats

```raku
method team_stats(
    Bool :h(:$http_body),
    *%params (Int :$team! where { ... }, Int :$league! where { ... }, Int :$season! where { ... }, Date(Any) :$date)
) returns Mu
```

Get the respone object containing team's statistics.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$team!

ID of the team

### ID :$league!

ID of the league

### Year :$season!

Season of the league

### Date() :$date

The limit date

## method team_seasons

```raku
method team_seasons(
    Bool :h(:$http_body),
    *%params (Int :$team! where { ... })
) returns Mu
```

Get the respone object containing list of available seasons for a team.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$team!

ID of the team

## method venues

```raku
method venues(
    Bool :h(:$http_body),
    *%params (Int :$id where { ... }, Str :$name, Str :$country, Str :$search where { ... })
) returns Mu
```

Get the respone object containing list of available venues.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$id

ID of the venue

### Str :$name

Name of the venue

### Str :$country

Country name of the venue

### SearchTerm :$search

Name, city or the country of the venue

## method tables

```raku
method tables(
    Bool :h(:$http_body),
    *%params (Int :$team where { ... }, Int :$league where { ... }, Int :$season! where { ... })
) returns Mu
```

Get the respone object containing standings of a league or team.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$team

ID of the team

### ID :$league

ID of the league

### Year :$season!

Season of the league

## method rounds

```raku
method rounds(
    Bool :h(:$http_body),
    *%params (Int :$league! where { ... }, Int :$season! where { ... }, Bool :$current)
) returns Mu
```

Get the respone object containing list of rounds for a league or cup.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$league!

ID of the league

### Year :$season!

Season of the league

### Bool :$current

Current round only

## method matches

```raku
method matches(
    Bool :h(:$http_body),
    *%params (Int :$id where { ... }, Str :$live where { ... }, Date(Any) :$date, Int :$league where { ... }, Int :$season where { ... }, Int :$team where { ... }, Int :$last where { ... }, Int :$next where { ... }, Date(Any) :$from, Date(Any) :$to, Str :$round, MatchStatus(Str) :$status, Str :$timezone where { ... })
) returns Mu
```

Get the respone object containing list of available matches.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$id

ID of the fixture

### AllOrIDs :$live

all or IDs of two teams

### Date() :$date

A valid date

### ID :$league

ID of the league

### Year :$season

Season of the league

### ID :$team

ID of the team

### AtMost2Digits :$last

For the X last fixtures

### AtMost2Digits :$next

For the X next fixtures

### Date() :$from

Fixtures from date

### Date() :$to

Fixtures to date

### Str :$round

Round of the fixture

### MatchStatus(Str) :$status

Status of the fixture

### Timezone :$timezone

A valid timezone

## method head_to_head

```raku
method head_to_head(
    Bool :h(:$http_body),
    *%params (Str :$h2h! where { ... }, Date(Any) :$date, Int :$league where { ... }, Int :$season where { ... }, Int :$last where { ... }, Int :$next where { ... }, Date(Any) :$from, Date(Any) :$to, MatchStatus(Str) :$status, Str :$timezone where { ... })
) returns Mu
```

Get the respone object containing list of head to heads between two teams.

### Bool :h(:$http_body)

Get the HTTP response body

### IDs :$h2h!

IDs of two teams

### Date() :$date

Date of the fixtures

### ID :$league

ID of the league

### Year :$season

Season of the league

### AtMost2Digits :$last

For the X last fixtures

### AtMost2Digits :$next

For the X next fixtures

### Date() :$from

Fixtures from date

### Date() :$to

Fixtures to date

### MatchStatus(Str) :$status

Status of the fixture

### Timezone :$timezone

A valid timezone

## method match_stats

```raku
method match_stats(
    Bool :h(:$http_body),
    *%params (Int :$fixture! where { ... }, Int :$team where { ... }, MatchStats(Str) :$type)
) returns Mu
```

Get the respone object containing statistics of a match.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$fixture!

ID of the fixture

### ID :$team

ID of the team

### MatchStats(Str) :$type

Type of statistics

## method fixture_events

```raku
method fixture_events(
    Bool :h(:$http_body),
    *%params (Int :$fixture! where { ... }, Int :$team where { ... }, Int :$player where { ... }, FixtureEvent(Str) :$type)
) returns Mu
```

Get the respone object containing events for a fixture.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$fixture!

ID of the fixture

### ID :$team

ID of the team

### ID :$player

ID of the player

### FixtureEvent(Str) :$type

Type of events

## method fixture_lineups

```raku
method fixture_lineups(
    Bool :h(:$http_body),
    *%params (Int :$fixture! where { ... }, Int :$team where { ... }, Int :$player where { ... }, FixtureLineup(Str) :$type)
) returns Mu
```

Get the respone object containing lineups for a fixture.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$fixture!

ID of the fixture

### ID :$team

ID of the team

### ID :$player

ID of the player

### FixtureEvent(Str) :$type

Type of lineups

## method fixture_players

```raku
method fixture_players(
    Bool :h(:$http_body),
    *%params (Int :$fixture! where { ... }, Int :$team where { ... })
) returns Mu
```

Get the respone object containing statistics for players from a fixture.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$fixture!

ID of the fixture

### ID :$team

ID of the team

## method injuries

```raku
method injuries(
    Bool :h(:$http_body),
    *%params (Int :$league where { ... }, Int :$fixture where { ... }, Int :$team where { ... }, Int :$player where { ... }, Date(Any) :$date, Int :$season where { ... }, Str :$timezone where { ... })
) returns Mu
```

Get the respone object containing list of players not participating in matches.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$league

ID of the league

### ID :$fixture

ID of the fixture

### ID :$team

ID of the team

### ID :$player

ID of the player

### Date() :$date

Date of the injury

### Year :$season

Season of the league, required with league, team and player parameters

### Timezone :$timezone

A valid timezone

## method predictions

```raku
method predictions(
    Bool :h(:$http_body),
    *%params (Int :$fixture! where { ... })
) returns Mu
```

Get the respone object containing predictions for a fixture.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$fixture!

ID of the fixture

## method coachs

```raku
method coachs(
    Bool :h(:$http_body),
    *%params (Int :$id where { ... }, Int :$team where { ... }, Str :$search where { ... })
) returns Mu
```

Get the respone object containing information about coachs and their careers.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$id

ID of the coach

### ID :$team

ID of the team

### SearchTerm :$search

Name of the coach

## method players

```raku
method players(
    Bool :h(:$http_body),
    *%params (Int :$id where { ... }, Int :$team where { ... }, Int :$league where { ... }, Int :$season where { ... }, Str :$search where { ... }, Int :$page)
) returns Mu
```

Get the respone object containing information and statistics about players.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$id

ID of the player

### ID :$team

ID of the team

### ID :$league

ID of the league

### Year :$season

Season of the league

### SearchTerm :$search

Name of the player

### Int :$page

Use for pagination

## method player_seasons

```raku
method player_seasons(
    Bool :h(:$http_body),
    *%params (Int :$player where { ... })
) returns Mu
```

Get the respone object containing list of available seasons for players statistics.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$player

ID of the player

## method squads

```raku
method squads(
    Bool :h(:$http_body),
    *%params (Int :$player where { ... }, Int :$team where { ... })
) returns Mu
```

Get the respone object containing squad of a team or squads of a player.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$player

ID of the player

### ID :$team

ID of the team

## method top_scorers

```raku
method top_scorers(
    Bool :h(:$http_body),
    *%params (Int :$league! where { ... }, Int :$season! where { ... })
) returns Mu
```

Get the respone object containing best players for a league or cup.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$league!

ID of the league

### Year :$season!

Season of the league

## method top_assists

```raku
method top_assists(
    Bool :h(:$http_body),
    *%params (Int :$league! where { ... }, Int :$season! where { ... })
) returns Mu
```

Get the respone object containing best assists for a league or cup.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$league!

ID of the league

### Year :$season!

Season of the league

## method top_yellow_cards

```raku
method top_yellow_cards(
    Bool :h(:$http_body),
    *%params (Int :$league! where { ... }, Int :$season! where { ... })
) returns Mu
```

Get the respone object containing players with the most yellow cards for a league or cup.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$league!

ID of the league

### Year :$season!

Season of the league

## method top_red_cards

```raku
method top_red_cards(
    Bool :h(:$http_body),
    *%params (Int :$league! where { ... }, Int :$season! where { ... })
) returns Mu
```

Get the respone object containing players with the most red cards for a league or cup.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$league!

ID of the league

### Year :$season!

Season of the league

## method transfers

```raku
method transfers(
    Bool :h(:$http_body),
    *%params (Int :$player where { ... }, Int :$team where { ... })
) returns Mu
```

Get the respone object containing all available transfers for players and teams.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$player

ID of the player

### ID :$team
----------

ID of the team

## method trophies

```raku
method trophies(
    Bool :h(:$http_body),
    *%params (Int :$player where { ... }, Int :$coach where { ... })
) returns Mu
```

Get the respone object containing all available trophies for a player or coach.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$player

ID of the player

### ID :$coach

ID of the coach

## method sidelined

```raku
method sidelined(
    Bool :h(:$http_body),
    *%params (Int :$player where { ... }, Int :$coach where { ... })
) returns Mu
```

Get the respone object containing all available sidelined information for a player or coach.

### Bool :h(:$http_body)

Get the HTTP response body

### ID :$player

ID of the player

### ID :$coach

ID of the coach

ERRORS
======

  * If a response contains errors, a `Failure` of type `X::APISports::Football::APIError` is returned.

  * If HTTP errors occur, `Failure` of type `X::APISports::Football::HTTPError` is returned.

REPOSITORY
==========

[https://codeberg.org/CIAvash/APISports-Football](https://codeberg.org/CIAvash/APISports-Football)

BUGS
====

[https://codeberg.org/CIAvash/APISports-Football/issues](https://codeberg.org/CIAvash/APISports-Football/issues)

AUTHOR
======

Siavash Askari Nasr [https://siavash.askari-nasr.com](https://siavash.askari-nasr.com)

COPYRIGHT
=========

Copyright © 2022 Siavash Askari Nasr

LICENSE
=======

APISports::Football is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

APISports::Football is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with APISports::Football. If not, see <http://www.gnu.org/licenses/>.
