=TITLE APISports::Football::Competition::Season::Coverage

=NAME APISports::Football::Competition::Season::Coverage

use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

=CLASSES

#| Class representing coverage of a league
unit class APISports::Football::Competition::Season::Coverage:auth($?DISTRIBUTION.meta<auth>) does JSON::Class;

#| Fixture coverage
class Fixture:auth($?DISTRIBUTION.meta<auth>) {
    #| Whether fixture has events coverage
    has Bool $.events;
    #| Whether fixture has lineups coverage
    has Bool $.lineups;
    #| Whether fixture has statistics coverage
    has Bool $.statistics_fixtures;
    #| Whether fixture has players statistics coverage
    has Bool $.statistics_players;
}

=ATTRIBUTES

#| Fixture coverage
has Fixture $.fixtures;
#| Whether league has standings coverage
has Bool $.standings;
#| Whether league has players coverage
has Bool $.players;
#| Whether league has top scorers coverage
has Bool $.top_scorers;
#| Whether league has top assists coverage
has Bool $.top_assists;
#| Whether league has top cards coverage
has Bool $.top_cards;
#| Whether league has injuries coverage
has Bool $.injuries;
#| Whether league has predictions coverage
has Bool $.predictions;
#| Whether league has odds coverage
has Bool $.odds;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
