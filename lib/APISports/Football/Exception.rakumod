=TITLE APISports::Football::Exception

=NAME APISports::Football::Exception

=DESCRIPTION Exception classes used for API and HTTP errors

use v6.d;

use APISports::Football::Response:auth($?DISTRIBUTION.meta<auth>);

unit package X::APISports::Football:auth($?DISTRIBUTION.meta<auth>);

=CLASSES

#| Used when the response returned by API contains errors
class APIError:auth($?DISTRIBUTION.meta<auth>) is Exception {
    #| The response body returned by API
    has APISports::Football::Response $.payload;

    #| Returns the errors field of response body, joined with newline
    method message returns Str {
        $!payload.errors.values.join: "\n"
    }
}

#| Used when the HTTP request results in error
class HTTPError:auth($?DISTRIBUTION.meta<auth>) is Exception {
    #| The response returned by the API, containing the error message and the HTTP response
    has APISports::Football::Response $.payload;

    #| Returns the response message
    method message returns Str {
        $!payload.message
    }
}

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
