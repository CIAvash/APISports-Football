=TITLE APISports::Football::Match

=NAME APISports::Football::Match

use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

use APISports::Football::Fixture:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :Goal;

=CLASSES

#| Class representing a match
unit class APISports::Football::Match:auth($?DISTRIBUTION.meta<auth>) does JSON::Class;

#| Team participating a match
class Team:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::Team {
    #| Whether the team is winner
    has Bool $.winner;
}

#| League the match belongs to
class League:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::CountryLeague {
    has Str $.round;
}

#| Teams participating a match
class Teams:auth($?DISTRIBUTION.meta<auth>) {
    #| Home team
    has Team $.home;
    #| Away team
    has Team $.away;
}

#| Score information of a match
class Score:auth($?DISTRIBUTION.meta<auth>) {
    has Goal $.halftime;
    has Goal $.fulltime;
    has Goal $.extratime;
    has Goal $.penalty;
}

=ATTRIBUTES

#| Fixture information
has APISports::Football::Fixture $.fixture handles *;
#| League information
has League $.league;
#| Teams information
has Teams $.teams;
#| Goals information
has Goal $.goals;
#| Score information
has Score $.score;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
