=TITLE APISports::Football::Trophy

=NAME APISports::Football::Trophy

use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

#| Class representing a trophy
unit class APISports::Football::Trophy:auth($?DISTRIBUTION.meta<auth>) does JSON::Class;

=ATTRIBUTES

#| Trophy's league
has Str $.league;
#| Trophy's country
has Str $.country;
#| Trophy's season
has Str $.season;
#| Trophy's place
has Str $.place;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
