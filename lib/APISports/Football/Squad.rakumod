=TITLE APISports::Football::Squad

=NAME APISports::Football::Squad

use v6.d;

use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :Team;

=CLASSES

#| Class representing an Squad
unit class APISports::Football::Squad:auth($?DISTRIBUTION.meta<auth>);

#| Squad player
class Player:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::Person {
    #| Player's age
    has Int $.age;
    #| Player's number
    has Int $.number;
    #| Player's position
    has Str $.position;
}

=ATTRIBUTES

#| Squad team
has Team $.team;
#| Squad players
has Player @.players;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
