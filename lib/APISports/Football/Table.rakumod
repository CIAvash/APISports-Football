=TITLE APISports::Football::Table

=NAME APISports::Football::Table

use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

use APISports::Football::Role::CountryLeague:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Role::HasUpdate:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :Team;
use APISports::Football::Utility:auth($?DISTRIBUTION.meta<auth>);

=CLASSES

#| Class representing table of a competition
unit class APISports::Football::Table:auth($?DISTRIBUTION.meta<auth>) does JSON::Class;

#| League of table
class League:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::CountryLeague does JSON::Class {
    #| Standing of league
    class Standing:auth($?DISTRIBUTION.meta<auth>) does JSON::Class {
        also does APISports::Football::Role::HasUpdate;

        #| Standing statistics
        class Stats:auth($?DISTRIBUTION.meta<auth>) {
            #| Goal statistics
            class Goal:auth($?DISTRIBUTION.meta<auth>) {
                #| Number of goals for
                has Int $.for;
                #| Number of goals against
                has Int $.against;
            }

            #| Number of games played
            has Int $.played;
            #| Number of wins
            has Int $.win;
            #| Number of draws
            has Int $.draw;
            #| Number of loses
            has Int $.lose;
            #| Goals
            has Goal $.goals;
        }

        #| Team's rank
        has Int $.rank;
        #| Team of the standing
        has Team $.team;
        #| Team's points
        has Int $.points;
        #| Team's goals difference
        has Int $.goals_diff is json-name<goalsDiff>;
        #| Team's group
        has Str $.group;
        #| Team's form
        has Str $.form;
        #| Team's status
        has Str $.status;
        #| Team's description
        has Str $.description;
        #| All team stats
        has Stats $.all;
        #| Team stats home
        has Stats $.home;
        #| Team stats away
        has Stats $.away;
    }

    #| Standings
    has Array[Standing] @.standings is unmarshalled-by(make_array_of_arrays(Standing));
}

=ATTRIBUTES

#| League of table
has League $.league;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
