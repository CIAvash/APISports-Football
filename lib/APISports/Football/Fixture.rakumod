=TITLE APISports::Football::Fixture

=NAME APISports::Football::Fixture

use v6.d;

use JSON::Unmarshal;

use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :Venue, :MatchStatus;
use APISports::Football::Utility:auth($?DISTRIBUTION.meta<auth>);

=CLASSES

#| Class representing a fixture
unit class APISports::Football::Fixture:auth($?DISTRIBUTION.meta<auth>);

also does APISports::Football::Role::Fixture;

#| Period of fixture
class Period:auth($?DISTRIBUTION.meta<auth>) {
    #| First period
    has Instant $.first  is unmarshalled-by(&make_instant);
    #| Second period
    has Instant $.second is unmarshalled-by(&make_instant);
}

#| Fixture status
class Status:auth($?DISTRIBUTION.meta<auth>) {
    #| Long status name
    has Str $.long;
    #| Short status name
    has Str $.short;
    #| Elapsed time
    has Int $.elapsed;
    #| Type of fixure status
    has MatchStatus $.type = coerce MatchStatus, $!short.uc;
}

=ATTRIBUTES

#| Referee of fixture
has Str $.referee;
#| Periods of fixture
has Period $.periods;
#| Venue of fixture
has Venue $.venue;
#| Status of fixture
has Status $.status;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
