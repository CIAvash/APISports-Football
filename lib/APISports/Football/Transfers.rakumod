=TITLE APISports::Football::Transfers

=NAME APISports::Football::Transfers

use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :Player, :Team;
use APISports::Football::Role::HasUpdate:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Role::HasDate:auth($?DISTRIBUTION.meta<auth>);

=CLASSES

#| Class representing transfers
unit class APISports::Football::Transfers:auth($?DISTRIBUTION.meta<auth>) does JSON::Class;

also does APISports::Football::Role::HasUpdate;

#| Class representing a transfer
class Transfer:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::HasDate {
    #| Teams involved in a transfer
    class Teams:auth($?DISTRIBUTION.meta<auth>) {
        #| Team player transfered in
        has Team $.in;
        #| Team player transfered out
        has Team $.out;
    }

    #| Type of transfer
    has Str $.type;
    #| Teams involved in transfer
    has Teams $.teams;
}

=ATTRIBUTES

#| Transfered player
has Player $.player;
#| Transfers
has Transfer @.transfers;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
