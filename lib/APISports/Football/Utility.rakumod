=TITLE APISports::Football::Utility

=NAME APISports::Football::Utility

use v6.d;

use JSON::Fast:ver<0.17.*>;

use APISports::Football::Fixture::Stats:auth($?DISTRIBUTION.meta<auth>);

#| Module containing utility and unmarshal functions
unit module APISports::Football::Utility:auth($?DISTRIBUTION.meta<auth>);

=SUBS

#| Given a posix C<Int>, returns an C<Instant>
proto make_instant (|) is export {*}
multi make_instant (Int:D $posix --> Instant) { Instant.from-posix: $posix }
multi make_instant ($ --> Instant) {}

#| Given an C<Str> datetime, returns a C<DateTime>
proto make_datetime (|) is export {*}
multi make_datetime (Str:D $datetime --> DateTime) { DateTime.new: $datetime }
multi make_datetime ($ --> DateTime) {}

#| Given an C<Str> date, returns a C<Date>
proto make_date (|) is export {*}
multi make_date (Str:D $date --> Date) { Date.new: $date }
multi make_date ($ --> Date) {}

#| Given a value, coerces it to type C<T>
proto coerce (|) is export {*}
multi coerce (::T, Any:D $value) { T($value) }
multi coerce (::T, Any:U --> T) {}

#| Given a list of fixture stats, modifies the items and returns C<APISports::Football::Fixture::Stats>
proto make_fixture_stats (|) is export {*}
multi make_fixture_stats (@fixture_stats --> APISports::Football::Fixture::Stats:D) {
    my %stats is Map = @fixture_stats.map: {

        my $type = .<type>.lc.trans: [' ', '%', 'insidebox', 'outsidebox'] => ['_', 'percentage', 'inside_box', 'outside_box'];

        $type => .<value> // Nil
    }

    APISports::Football::Fixture::Stats.new: |%stats
}
multi make_fixture_stats ($ --> APISports::Football::Fixture::Stats) {}

#| Given a type, returns a function that takes an C<Array> of arrays and returns C<Array[Array[type]]>
sub make_array_of_arrays (Any:U $class --> Code:D) is export {
    -> @array {
        if @array {
            Array[Array[$class]].new: @array.map: { Array[$class].new: .map: { $class.from-json: .&to-json } }
        } else {
            Array[Array[$class]]
        }
    }
}

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
