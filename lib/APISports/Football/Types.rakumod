=TITLE APISports::Football::Types

=NAME APISports::Football::Types

use v6.d;

use experimental :will-complain;

use APISports::Football::Role::Team:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Role::Person:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Role::Goal:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Role::Venue:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Role::Fixture:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Role::CountryLeague:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Role::Card:auth($?DISTRIBUTION.meta<auth>);

#| Module containing classes, subsets, enums, ...
unit module APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>);

=TYPES

#| Class representing a team
class Team:auth($?DISTRIBUTION.meta<auth>)          does APISports::Football::Role::Team          is export(:Team)          {}
#| Class representing a player
class Player:auth($?DISTRIBUTION.meta<auth>)        does APISports::Football::Role::Person        is export(:Player)        {}
#| Class representing a coach
class Coach:auth($?DISTRIBUTION.meta<auth>)         does APISports::Football::Role::Person        is export(:Couch)         {}
#| Class representing a goal
class Goal:auth($?DISTRIBUTION.meta<auth>)          does APISports::Football::Role::Goal          is export(:Goal)          {}
#| Class representing a venue
class Venue:auth($?DISTRIBUTION.meta<auth>)         does APISports::Football::Role::Venue         is export(:Venue)         {}
#| Class representing a league
class League:auth($?DISTRIBUTION.meta<auth>)        does APISports::Football::Role::League        is export(:League)        {}
#| Class representing a league that has a country, flag and season
class CountryLeague:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::CountryLeague is export(:CountryLeague) {}
#| Class representing a fixture
class Fixture:auth($?DISTRIBUTION.meta<auth>)       does APISports::Football::Role::Fixture       is export(:Fixture)       {}
#| Class representing a card
class Card:auth($?DISTRIBUTION.meta<auth>)          does APISports::Football::Role::Card          is export(:Card)          {}

subset Timezone of Str is export(:Timezone, :subsets);

subset ID of Int is export(:ID, :subsets);

subset Year of Int is export(:Year, :subsets)
       will complain { .&note_error: 'must be Int and 4 digits' }
       where !.defined || .chars == 4;

subset AtMost2Digits of Int is export(:AtMost2Digits, :subsets)
       will complain { .&note_error: 'must be Int and at most 2 digits' }
       where !.defined || .chars ≤ 2;

subset TwoChars of Str is export(:TwoChars, :subsets)
       will complain { .&note_error: 'must be Str and 2 characters long' }
       where !.defined || .chars == 2;

subset SearchTerm of Str is export(:SearchTerm, :subsets)
       will complain { .&note_error: 'must be Str and at least 3 characters long' }
       where !.defined || .chars ≥ 3;

my token id { <[0..9]>+ }
subset AllOrIDs of Str is export(:AllOrIDs, :subsets)
       will complain { .&note_error: ｢must be Str and 'all' or IDs of form id-id-*｣ }
       where !.defined || 'all' | /<id>+ % '-'/;

subset IDs of Str is export(:IDs, :subsets)
       will complain { .&note_error: 'must be Str and IDs of form id-id' }
       where !.defined || /<id> '-' <id>/;

#| Enum C<LeagueType>
constant LeagueType is export(:LeagueType, :types) = {
    enum LeagueType (
        League => 'league',
        Cup    => 'cup',
    );

    LeagueType
}();

#| Enum C<MatchStatus>
constant MatchStatus is export(:MatchStatus, :types) = {
    enum MatchStatus (
        TimeToBeDefined        => 'TBD',
        NotStarted             => 'NS',
        FirstHalf              => '1H',
        Halftime               => 'HT',
        SecondHalf             => '2H',
        Extratime              => 'ET',
        Penalty                => 'P',
        Finished               => 'FT',
        FinishedAfterExtratime => 'AET',
        FinishedAfterPenalty   => 'PEN',
        BreakTimeInExtratime   => 'BT',
        Suspended              => 'SUSP',
        Interruppted           => 'INT',
        Postponed              => 'PST',
        Cancelled              => 'CANC',
        Abondoned              => 'ABD',
        TechnicalLoss          => 'AWD',
        Walkover               => 'WO',
        InProgress             => 'LIVE',
    );

    MatchStatus
}();

#| Enum C<MatchStats>
constant MatchStats is export(:MatchStats, :types) = {
    enum MatchStats (
        ShotsOnGoal      => 'shots on goal',
        ShotsOffGoal     => 'shots off goal',
        ShotsInsideBox   => 'shots insidebox',
        ShotsOutsideBox  => 'shots outsidebox',
        TotalShots       => 'total shots',
        BlockedShots     => 'blocked shots',
        Fouls            => 'fouls',
        CornerKicks      => 'corner kicks',
        Offsides         => 'offsides',
        BallPossession   => 'ball possession',
        YellowCards      => 'yellow cards',
        RedCards         => 'red cards',
        GoalkeeperSaves  => 'goalkeeper saves',
        TotalPasses      => 'total passes',
        PassesAccurate   => 'passes accurate',
        PassesPercentage => 'passes %',
    );

    MatchStats
}();

#| Enum C<FixtureEvent>
constant FixtureEvent is export(:FixtureEvent, :types) = {
    enum FixtureEvent (
        Goal         => 'goal',
        Card         => 'card',
        Substitution => 'subst',
        VAR          => 'var',
    );

    FixtureEvent
}();

#| Enum C<FixtureLineup>
constant FixtureLineup is export(:FixtureLineup, :types) = {
    enum FixtureLineup (
        Formation   => 'formation',
        Coach       => 'coach',
        StartXI     => 'startxi',
        Substitutes => 'substitutes',
    );

    FixtureLineup
}();

=SUBS

#| Notes message and returns C<False>
sub note_error ($topic, Str $msg --> Str:D) {
    "$topic.^name() (\"$topic\") $msg"
}

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
