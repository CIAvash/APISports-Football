=TITLE APISports::Football::Player

=NAME APISports::Football::Player

use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

use APISports::Football::Role::Player:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Role::Profile:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :Team, :CountryLeague;

=CLASSES

#| Class representing a player and their statistics
unit class APISports::Football::Player:auth($?DISTRIBUTION.meta<auth>) does JSON::Class;

#| A class with attributes for a player
class Player:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::Profile {
    #| Is the player injured?
    has Bool $.injured;
}

#| Player statistics
class Stats:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::Player::Stats {
    #| Game statistics for a player
    class Game:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::Player::Stats::Game {
        #| Number of appearances
        has Int $.appearences;
        #| Number of being part of lineups
        has Int $.lineups;
    }

    #| Card statistics for a player
    class Card:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::Card {
        #| Number of yellow+red cards
        has Int $.yellowred;
    }

    #| Substitution statistics for a player
    class Substitute:auth($?DISTRIBUTION.meta<auth>) {
        #| Number of times substituted in
        has Int $.in;
        #| Number of times substituted out
        has Int $.out;
        #| Number of times on bench
        has Int $.bench;
    }

    #| Player's team
    has Team $.team;
    #| Player's league
    has CountryLeague $.league;
    #| Player's game statistics
    has Game $.games;
    #| Player's substitution statistics
    has Substitute $.substitutes;
    #| Player's card statistics
    has Card $.cards;
}

=ATTRIBUTES

#| Object containing player information
has Player $.player handles *;
#| Object containing player statistics
has Stats @.statistics;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
