=TITLE APISports::Football::Fixture::Stats

=NAME APISports::Football::Fixture::Stats

use v6.d;

#| Class representing fixture statistics
unit class APISports::Football::Fixture::Stats:auth($?DISTRIBUTION.meta<auth>);

has Int $.shots_on_goal;
has Int $.shots_off_goal;
has Int $.total_shots;
has Int $.blocked_shots;
has Int $.shots_inside_box;
has Int $.shots_outside_box;
has Int $.fouls;
has Int $.corner_kicks;
has Int $.offsides;
has Str $.ball_possession;
has Int $.yellow_cards;
has Int $.red_cards;
has Int $.goalkeeper_saves;
has Int $.total_passes;
has Int $.passes_accurate;
has Str $.passes_percentage;
