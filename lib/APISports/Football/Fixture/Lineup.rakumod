=TITLE APISports::Football::Fixture::Lineup

=NAME APISports::Football::Fixture::Lineup

use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

use APISports::Football::Role::Team:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Role::Person:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :Couch;

=CLASSES

#| Class representing a fixture lineup
unit class APISports::Football::Fixture::Lineup:auth($?DISTRIBUTION.meta<auth>) does JSON::Class;

#| Lineup team
class Team:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::Team {
    #| Team color
    class Color:auth($?DISTRIBUTION.meta<auth>) {
        #| Primary color
        has Str $.primary;
        #| Number color
        has Str $.number;
        #| Border color
        has Str $.border;
    }

    #| Team colors
    class Colors:auth($?DISTRIBUTION.meta<auth>) {
        #| Player color
        has Color $.player;
        #| Goalkeeper color
        has Color $.goalkeeper;
    }

    #| Team colors
    has Colors $.colors;
}

#| Player
class Player:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::Person {
    #| Player number
    has Int $.number;
    #| Player position
    has Str $.pos;
    #| Player grid position
    has Str $.grid;
}

#| Lineup player
class LineupPlayer:auth($?DISTRIBUTION.meta<auth>) {
    #| Player
    has Player $.player handles *;
}

=ATTRIBUTES

#| Lineup team
has Team $.team;
#| Lineup formation
has Str $.formation;
#| Lineup starting XI
has LineupPlayer @.startXI;
#| Lineup substitutes
has LineupPlayer @.substitutes;
#| Lineup coach
has Coach $.coach;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
