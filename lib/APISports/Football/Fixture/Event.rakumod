=TITLE APISports::Football::Fixture::Event

=NAME APISports::Football::Fixture::Event

use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :FixtureEvent;
use APISports::Football::Utility:auth($?DISTRIBUTION.meta<auth>);

=CLASSES

#| Class representing a fixture event
unit class APISports::Football::Fixture::Event:auth($?DISTRIBUTION.meta<auth>) does JSON::Class;

#| Event time
my class Time {
    #| Elapsed time
    has Int $.elapsed;
    #| Extra time?
    has Bool $.extra;
}

=ATTRIBUTES

#| Event time
has Time $.time;
#| Event team
has APISports::Football::Types::Team $.team;
#| Event player
has APISports::Football::Types::Player $.player;
#| Event assist player
has APISports::Football::Types::Player $.assist;
#| Event type
has FixtureEvent $.type is unmarshalled-by{ coerce FixtureEvent, .lc };
#| Event detail
has Str $.detail;
#| Event comments
has Str $.comments;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
