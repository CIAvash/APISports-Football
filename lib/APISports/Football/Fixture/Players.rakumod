=TITLE APISports::Football::Fixture::Players

=NAME APISports::Football::Fixture::Players

use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

use APISports::Football::Role::HasUpdate:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Role::Player:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :Player, :Card;

=CLASSES

#| Fixture players
unit class APISports::Football::Fixture::Players:auth($?DISTRIBUTION.meta<auth>) does JSON::Class;

#| Player statistics
class PlayerStats {
    #| Statistics
    class Stats:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::Player::Stats {
        #| Game statistics
        class Game:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::Player::Stats::Game {
            #| Is player a substitute?
            has Bool $.substitute;
        }

        #| Number of offsides
        has Int $.offsides;
        #| Game statistics
        has Game $.games;
        #| Cards
        has Card $.cards;
    }

    #| Player
    has Player $.player handles *;
    #| Statistics
    has Stats @.statistics;
}

#| Players' team
class Team:auth($?DISTRIBUTION.meta<auth>) {
    also does APISports::Football::Role::Team;
    also does APISports::Football::Role::HasUpdate;
}

=ATTRIBUTES

#| Players' team
has Team $.team;
#| Players' statistics
has PlayerStats @.players;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
