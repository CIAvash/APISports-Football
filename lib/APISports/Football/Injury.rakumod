=TITLE APISports::Football::Injury

=NAME APISports::Football::Injury

use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

use APISports::Football::Role::Person:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :Team, :League, :Fixture;

=CLASSES

#| Class representing why a player is not participating in a match
unit class APISports::Football::Injury:auth($?DISTRIBUTION.meta<auth>) does JSON::Class;

#| Injured player
class Player:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::Person {
    #| Type of injury
    has Str $.type;
    #| Reason of injury
    has Str $.reason;
}

#| Injured player
has Player $.player;
#| Injured player's team
has Team $.team;
#| Fixture involving injured player
has Fixture $.fixture;
#| League involving injured player
has League $.league;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
