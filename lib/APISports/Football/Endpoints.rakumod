=TITLE APISports::Football::Endpoints

=NAME APISports::Football::Endpoints

use v6.d;

use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :Timezone, :Year;
use APISports::Football::Country:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Competition:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Club:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Team::Stats:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Venue:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Table:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Match:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Match::Stats:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Fixture::Event:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Fixture::Lineup:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Fixture::Players:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Injury:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Match::Prediction:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Coach:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Player:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Squad:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Transfers:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Trophy:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Sidelined:auth($?DISTRIBUTION.meta<auth>);

#| Module containing API endpoints and their classes
unit module APISports::Football::Endpoints:auth($?DISTRIBUTION.meta<auth>);

#| Enum C<Endpoint> - each endpoint has a URL and class
constant Endpoint is export = {
    enum Endpoint (
        Timezones      => %(url => 'timezone',               class => Positional[Timezone]),
        Countries      => %(url => 'countries',              class => Positional[APISports::Football::Country]),
        Seasons        => %(url => 'leagues/seasons',        class => Positional[Year]),
        Competitions   => %(url => 'leagues',                class => Positional[APISports::Football::Competition]),
        Clubs          => %(url => 'teams',                  class => Positional[APISports::Football::Club]),
        TeamStats      => %(url => 'teams/statistics',       class => APISports::Football::Team::Stats),
        TeamSeasons    => %(url => 'teams/seasons',          class => Positional[Year]),
        Venues         => %(url => 'venues',                 class => Positional[APISports::Football::Venue]),
        Tables         => %(url => 'standings',              class => Positional[APISports::Football::Table]),
        Rounds         => %(url => 'fixtures/rounds',        class => Positional[Str]),
        Matches        => %(url => 'fixtures',               class => Positional[APISports::Football::Match]),
        HeadToHead     => %(url => 'fixtures/headtohead',    class => Positional[APISports::Football::Match]),
        MatchStats     => %(url => 'fixtures/statistics',    class => Positional[APISports::Football::Match::Stats]),
        FixtureEvents  => %(url => 'fixtures/events',        class => Positional[APISports::Football::Fixture::Event]),
        FixtureLineups => %(url => 'fixtures/lineups',       class => Positional[APISports::Football::Fixture::Lineup]),
        FixturePlayers => %(url => 'fixtures/players',       class => Positional[APISports::Football::Fixture::Players]),
        Injuries       => %(url => 'injuries',               class => Positional[APISports::Football::Injury]),
        Predictions    => %(url => 'predictions',            class => Positional[APISports::Football::Match::Prediction]),
        Coachs         => %(url => 'coachs',                 class => Positional[APISports::Football::Coach]),
        PlayerSeasons  => %(url => 'players/seasons',        class => Positional[Year]),
        Squads         => %(url => 'players/squads',         class => Positional[APISports::Football::Squad]),
        TopScorers     => %(url => 'players/topscorers',     class => Positional[APISports::Football::Player]),
        TopAssists     => %(url => 'players/topassists',     class => Positional[APISports::Football::Player]),
        TopYellowCards => %(url => 'players/topyellowcards', class => Positional[APISports::Football::Player]),
        TopRedCards    => %(url => 'players/topredcards',    class => Positional[APISports::Football::Player]),
        Players        => %(url => 'players',                class => Positional[APISports::Football::Player]),
        Transfers      => %(url => 'transfers',              class => Positional[APISports::Football::Transfers]),
        Trophies       => %(url => 'trophies',               class => Positional[APISports::Football::Trophy]),
        Sidelined      => %(url => 'sidelined',              class => Positional[APISports::Football::Sidelined]),
    );

    Endpoint
}();

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
