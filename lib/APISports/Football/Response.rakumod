=TITLE APISports::Football::Response

=NAME APISports::Football::Response

=DESCRIPTION Response roles used for API and HTTP responses and errors

use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

=ROLES

#| Common response fields
role APISports::Football::Response::Common:auth($?DISTRIBUTION.meta<auth>) does JSON::Class {
    #| Request endpoint
    has Str $.get;
    #| Request parameters
    has %.parameters;
    #| Request errors
    has $.errors;
    #| Request results
    has Int $.results;
    #| Request paging, used for pagination
    has %.paging;
}

#| Response containing C<Array> of C<ResponseClass>es
role APISports::Football::Response:auth($?DISTRIBUTION.meta<auth>) [Array, Any:U ::ResponseClass] {
    also does APISports::Football::Response::Common;

    #| Object containing the response field of the HTTP response body
    has ResponseClass @.response;
}

#| Response containing a C<ResponseClass>
role APISports::Football::Response:auth($?DISTRIBUTION.meta<auth>) [Any:U ::ResponseClass] {
    also does APISports::Football::Response::Common;

    #| Object containing the response field of the HTTP response body
    has ResponseClass $.response;
}

#| Response role for requests containing errors
role APISports::Football::Response:auth($?DISTRIBUTION.meta<auth>) [Any:D $http_error] does JSON::Class {
    #| The message field of response body returned by the API or HTTP error message
    has Str $.message = $http_error.Str;

    #| HTTP response returned by HTTP client or response body
    method http_response {
        $http_error.response
    }
}

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
