=TITLE APISports::Football::Team::Stats

=NAME APISports::Football::Team::Stats

use v6.d;

use APISports::Football::Role::Team::Stats:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :Team, :CountryLeague;

#| Class containing team statistics
unit class APISports::Football::Team::Stats:auth($?DISTRIBUTION.meta<auth>);

also does APISports::Football::Role::Team::Stats;

=ATTRIBUTES

has CountryLeague $.league;
has Team          $.team handles *;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
