=TITLE APISports::Football::Team

=NAME APISports::Football::Team

use v6.d;

use APISports::Football::Role::Team:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :Year;

#| Class representing a team
unit class APISports::Football::Team:auth($?DISTRIBUTION.meta<auth>);

also does APISports::Football::Role::Team;

=ATTRIBUTES

#| Team's code
has Str $.code;
#| Team's country
has Str $.country;
#| The year the team was founded
has Year $.founded;
#| Is the team a national team?
has Bool $.national;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
