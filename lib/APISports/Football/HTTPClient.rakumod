=TITLE APISports::Football::HTTPClient

=NAME APISports::Football::HTTPClient - Class for making HTTP requests to api-football.com

=begin SYNOPSIS

=begin code :lang<raku>

use APISports::Football::HTTPClient;

my $request = APISports::Football::HTTPClient.new:
    api_key => '12345',
    base_url => 'https://v3.football.api-sports.io/';

my @clubs = $request.get: Endpoints::Clubs, params => name => 'manchester city';

=end code

=end SYNOPSIS

use v6.d;

use Cro::HTTP::Client:ver<0.8.*>;
use JSON::Class:api<1.0>:version<0.0.*>;
use Cro::HTTP::BodyParser::JSONClass:ver<0.0.3>:auth<zef:jonathanstowe>:api<0.1>;

use APISports::Football::Exception:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Response:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Endpoints:auth($?DISTRIBUTION.meta<auth>);

#| Class that makes it easy to make HTTP requests to api-football.com
unit class APISports::Football::HTTPClient:auth($?DISTRIBUTION.meta<auth>);

#| Role Headers containing headers and rate limit information 
role Headers [@headers] {
    #| Class containing rate limit information
    my class RateLimit {
        #| Number of requests allocated per minute
        has Int $.per_minute;
        #| Number of requests remaining per minute
        has Int $.remaining_per_minute;
        #| Number of requests allocated per day
        has Int $.per_day;
        #| Number of requests remaining per day
        has Int $.remaining_per_day;
    }

    #| Returns list of headers
    method headers returns List {
        @headers
    }

    #| Returns RateLimit
    method rate_limit returns RateLimit:D {
        my %rate_limit is Map = @headers
                                .grep(*.name.contains: 'ratelimit')
                                .map: {
                                    my $name = do given .name {
                                        when 'x-ratelimit-limit'              { 'per_minute'           }
                                        when 'x-ratelimit-remaining'          { 'remaining_per_minute' }
                                        when 'x-ratelimit-requests-limit'     { 'per_day'              }
                                        when 'x-ratelimit-requests-remaining' { 'remaining_per_day'    }
                                        default { next }
                                    }
                                    $name => .value.Int
                                };

        RateLimit.new: |%rate_limit
    }
}

=ATTRIBUTES

#| API key provided by api-football.com
has Str:D $.api_key is required;
#| Base URL used for making HTTP requests
has Str:D $.base_url = 'https://v3.football.api-sports.io/';
#| Body parser to be used with Cro::HTTP::Client
has Cro::HTTP::BodyParser::JSONClass $.body_parser .= new;

#| Useragent for making HTTP requests
has Cro::HTTP::Client $.ua .= new: base-uri     => $!base_url,
                                   headers      => [x-apisports-key => $!api_key],
                                   body-parsers => [$!body_parser];

=METHODS

proto method get (Endpoint:D $endpoint, Bool :h(:$http_body), :%params) {*}

#| Makes HTTP requests to api-football.com and returns the body of response.
#| If response has errors, a C<Failure> containing a payload is returned.
multi method get ($endpoint, Bool:D :h(:$http_body) where :so, :%params --> APISports::Football::Response:D) {
    $!body_parser.set-json-class: do given $endpoint<class> {
        when Positional {
            APISports::Football::Response[Array, .of]
        }
        default {
            APISports::Football::Response[$_]
        }
    }

    CATCH {
        when X::Cro::HTTP::Error {
            $!body_parser.set-json-class: APISports::Football::Response[$_];
            return fail X::APISports::Football::HTTPError.new: payload => await .response.body;
        }
    }

    my $http_response = await $!ua.get: $endpoint<url>, query => %params;

    given $http_response.body.&await does Headers[$http_response.headers] {
        .errors ?? X::APISports::Football::APIError.new(payload => $_).fail !! $_
    }
}

#| Makes HTTP requests to api-football.com and returns the response property of the response body.
#| If response has errors, a C<Failure> containing a payload is returned.
multi method get ($endpoint, Bool :h(:$http_body) where :not, |c) {
    with samewith $endpoint, |c, :http_body {
        .response
    } else {
        .handled = False;
        $_
    }
}

=ERRORS
=item If a response contains errors, a C<Failure> of type C<X::APISports::Football::APIError> is returned.
=item If HTTP errors occur, C<Failure> of type C<X::APISports::Football::HTTPError> is returned.

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
