=TITLE APISports::Football::Role::Profile

=NAME APISports::Football::Role::Profile

use v6.d;

use JSON::Unmarshal;

use APISports::Football::Role::Person:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Role::HasDate:auth($?DISTRIBUTION.meta<auth>);

#| Role representing profile of a person
unit role APISports::Football::Role::Profile:auth($?DISTRIBUTION.meta<auth>);

also does APISports::Football::Role::Person;

=CLASSES

my class Birth:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::HasDate {
    has Str $.place;
    has Str $.country;
}

=ATTRIBUTES

has Str   $.firstname;
has Str   $.lastname;
has Int   $.age;
has Birth $.birth;
has Str   $.nationality;
has Str   $.height;
has Str   $.weight;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
