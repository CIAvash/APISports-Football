=TITLE APISports::Football::Role::Fixture

=NAME APISports::Football::Role::Fixture

use v6.d;

use JSON::Unmarshal;

use APISports::Football::Utility:auth($?DISTRIBUTION.meta<auth>);

#| Role representing a fixture
unit role APISports::Football::Role::Fixture:auth($?DISTRIBUTION.meta<auth>);

=ATTRIBUTES

has Int      $.id;
has Str      $.timezone;
has DateTime $.date      is unmarshalled-by(&make_datetime);
has Instant  $.timestamp is unmarshalled-by(&make_instant);

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
