=TITLE APISports::Football::Role::Team::Stats

=NAME APISports::Football::Role::Team::Stats

use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

use APISports::Football::Role::Goal:auth($?DISTRIBUTION.meta<auth>);

#| Role representing team statistics
unit role APISports::Football::Role::Team::Stats:auth($?DISTRIBUTION.meta<auth>);

=CLASSES

my class Fixtures:auth($?DISTRIBUTION.meta<auth>) {
    class Fixture:auth($?DISTRIBUTION.meta<auth>) {
        has Int $.home;
        has Int $.away;
        has Int $.total;
    }

    has Fixture $.played;
    has Fixture $.wins;
    has Fixture $.draws;
    has Fixture $.loses;
}

my class TotalStats:auth($?DISTRIBUTION.meta<auth>) {
    has Int $.total;
    has Str $.percentage;
}

my class Goals:auth($?DISTRIBUTION.meta<auth>) {
    class Stats:auth($?DISTRIBUTION.meta<auth>) {
        class Goal:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::Goal {
            has Int $.total;
        }

        class Average:auth($?DISTRIBUTION.meta<auth>) {
            has Rat $.home;
            has Rat $.away;
            has Rat $.total;
        }

        class Minute:auth($?DISTRIBUTION.meta<auth>) {
            has TotalStats $._0_15    is json-name<0-15>;
            has TotalStats $._16_30   is json-name<16-30>;
            has TotalStats $._31_45   is json-name<31-45>;
            has TotalStats $._46_60   is json-name<46-60>;
            has TotalStats $._61_75   is json-name<61-75>;
            has TotalStats $._76_90   is json-name<76-90>;
            has TotalStats $._91_105  is json-name<91-105>;
            has TotalStats $._106_120 is json-name<106-120>;
        }

        has Goal $.total;
        has Average $.average;
        has Minute $.minute;
    }
    
    has Stats $.for;
    has Stats $.against;
}

my class Biggest:auth($?DISTRIBUTION.meta<auth>) {
    class Streak:auth($?DISTRIBUTION.meta<auth>) {
        has Int $.wins;
        has Int $.draws;
        has Int $.loses;
    }
    
    class Results:auth($?DISTRIBUTION.meta<auth>) {
        has Str $.home;
        has Str $.away;
    }

    class Goals:auth($?DISTRIBUTION.meta<auth>) {
        class Goal:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::Goal {}

        has Goal $.for;
        has Goal $.against;
    }

    has Streak $.streak;
    has Results $.wins;
    has Results $.loses;
    has Goals $.goals;
}

my class Streak:auth($?DISTRIBUTION.meta<auth>) {
    has Int $.home;
    has Int $.away;
    has Int $.total;
}

my class Penalty:auth($?DISTRIBUTION.meta<auth>) {
    has TotalStats $.scored;
    has TotalStats $.missed;
    has Int $.total;
}

my class Lineup:auth($?DISTRIBUTION.meta<auth>) {
    has Str $.formation;
    has Int $.played;
}

my class Card:auth($?DISTRIBUTION.meta<auth>) {
    has %.yellow;
    has %.red;
}

=ATTRIBUTES

has Str      $.form;
has Fixtures $.fixtures;
has Goals    $.goals;
has Biggest  $.biggest;
has Streak   $.clean_sheet;
has Streak   $.failed_to_score;
has Penalty  $.penalty;
has Lineup   @.lineups;
has Card     $.cards;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
