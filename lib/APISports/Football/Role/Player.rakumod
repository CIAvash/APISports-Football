=TITLE APISports::Football::Role::Player

=NAME APISports::Football::Role::Player

use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

#| Package containing player roles
unit package APISports::Football::Role::Player:auth($?DISTRIBUTION.meta<auth>);

role Stats:auth($?DISTRIBUTION.meta<auth>) {
    my class Shot:auth($?DISTRIBUTION.meta<auth>) {
        has Int $.total;
        has Int $.on;
    }

    my class Goal:auth($?DISTRIBUTION.meta<auth>) {
        has Int $.total;
        has Int $.conceded;
        has Int $.assists;
        has Int $.saves;
    }

    my class Pass:auth($?DISTRIBUTION.meta<auth>) {
        has Int $.total;
        has Int $.key;
        has Rat $.accuracy;
    }

    my class Tackle:auth($?DISTRIBUTION.meta<auth>) {
        has Int $.total;
        has Int $.blocks;
        has Int $.interceptions;
    }

    my class Duel:auth($?DISTRIBUTION.meta<auth>) {
        has Int $.total;
        has Int $.won;
    }

    my class Dribble:auth($?DISTRIBUTION.meta<auth>) {
        has Int $.attempts;
        has Int $.success;
        has Int $.past;
    }

    my class Foul:auth($?DISTRIBUTION.meta<auth>) {
        has Int $.drawn;
        has Int $.committed;
    }

    my class Penalty:auth($?DISTRIBUTION.meta<auth>) {
        has Int $.won;
        has Int $.committed is json-name<commited>;
        has Int $.scored;
        has Int $.missed;
        has Int $.saved;
    }

    has Shot    $.shots;
    has Goal    $.goals;
    has Pass    $.passes;
    has Tackle  $.tackles;
    has Duel    $.duels;
    has Dribble $.dribbles;
    has Foul    $.fouls;
    has Penalty $.penalty;
}

role Stats::Game:auth($?DISTRIBUTION.meta<auth>) {
    has Int  $.minutes;
    has Int  $.number;
    has Str  $.position;
    has Rat  $.rating;
    has Bool $.captain;
}

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
