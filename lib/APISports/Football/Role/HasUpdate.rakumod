=TITLE APISports::Football::Role::HasUpdate

=NAME APISports::Football::Role::HasUpdate

use v6.d;

use JSON::Unmarshal;

use APISports::Football::Utility:auth($?DISTRIBUTION.meta<auth>);

#| Role containing a update attribute
unit role APISports::Football::Role::HasUpdate:auth($?DISTRIBUTION.meta<auth>);

=ATTRIBUTES

has DateTime $.update is unmarshalled-by(&make_datetime);

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
