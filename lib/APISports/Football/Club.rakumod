=TITLE APISports::Football::Club

=NAME APISports::Football::Club

use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

use APISports::Football::Venue:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Team:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :subsets;

#| Class representing a club containing its team and venue
unit class APISports::Football::Club:auth($?DISTRIBUTION.meta<auth>) does JSON::Class;

=ATTRIBUTES

#| Object containing team information
has APISports::Football::Team $.team handles *;
#| Object containing venue information
has APISports::Football::Venue $.venue;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
