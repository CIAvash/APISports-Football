=TITLE APISports::Football::Match::Prediction

=NAME APISports::Football::Match::Prediction

use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

use APISports::Football::Role::Team::Stats:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Match:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :League;

=CLASSES

#| Class representing match predictions
unit class APISports::Football::Match::Prediction:auth($?DISTRIBUTION.meta<auth>) does JSON::Class;

#| Match predictions
class Predictions:auth($?DISTRIBUTION.meta<auth>) {
    #| Winner team
    class Winner:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::Team {
        has Str $.comment;
    }

    #| Goals
    class Goals:auth($?DISTRIBUTION.meta<auth>) {
        has Rat $.home;
        has Rat $.away;
    }

    #| Percentage
    class Percent:auth($?DISTRIBUTION.meta<auth>) {
        has Str $.home;
        has Str $.draw;
        has Str $.away;        
    }

    has Winner $.winner;
    has Bool $.win_or_draw;
    has Rat $.under_over;
    has Goals $.goals;
    has Str $.advice;
    has Percent $.percent;
}

#| Match teams
class Teams:auth($?DISTRIBUTION.meta<auth>) {
    #| Team
    class Team:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::Team {
        #| Last 5 stats
        class Last5:auth($?DISTRIBUTION.meta<auth>) {
            class Goals:auth($?DISTRIBUTION.meta<auth>) {
                class Stats:auth($?DISTRIBUTION.meta<auth>) {
                    has Int $.total;
                    has Rat $.average;
                }
                
                has Stats $.for;
                has Stats $.against;
            }

            has Str $.form;
            has Str $.att;
            has Str $.def;
            has Goals $.goals;
        }

        #| League
        class League:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::Team::Stats {} 

        #| Last 5 stats
        has Last5 $.last_5;
        #| League
        has League $.league;
    }

    has Team $.home;
    has Team $.away;
}

#| Match comparison
class Comparison:auth($?DISTRIBUTION.meta<auth>) {
    class Stats:auth($?DISTRIBUTION.meta<auth>) {
        has Str $.home;
        has Str $.away;
    }

    has Stats $.form;
    has Stats $.att;
    has Stats $.def;
    has Stats $.poisson_distribution;
    has Stats $.h2h;
    has Stats $.goals;
    has Stats $.total;
}

=ATTRIBUTES

#| Match predictions
has Predictions $.predictions;
#| Match league
has League $.league;
#| Match teams
has Teams $.teams;
#| Teams' head to heads
has APISports::Football::Match @.h2h;
#| Teams comparison
has Comparison $.comparison;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
