use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

use APISports::Football::Fixture::Stats:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Utility:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :Team;

unit class APISports::Football::Match::Stats:auth($?DISTRIBUTION.meta<auth>) does JSON::Class;

has Team $.team;
has APISports::Football::Fixture::Stats $.statistics is unmarshalled-by(&make_fixture_stats) handles *;
