=TITLE APISports::Football::Coach

=NAME APISports::Football::Coach

use v6.d;

use JSON::Class:api<1.0>:version<0.0.*>;

use APISports::Football::Role::Profile:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Role::HasStartEnd:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :Team;

=CLASSES

#| Class representing a coach containing their team and career
unit class APISports::Football::Coach:auth($?DISTRIBUTION.meta<auth>) does JSON::Class;

also does APISports::Football::Role::Profile;

#| Class representing career of a coach
class Career:auth($?DISTRIBUTION.meta<auth>) does APISports::Football::Role::HasStartEnd {
    #| Team which the coach managed
    has Team $.team;
}

=ATTRIBUTES

#| Team which the coach is currently managing
has Team $.team;
#| Career of the coach
has Career @.career;

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
