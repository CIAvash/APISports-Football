=TITLE APISports::Football

=NAME APISports::Football - An interface to L<API-Football|https://www.api-football.com/>.

=begin DESCRIPTION
APISports::Football is a L<Raku|https://www.raku-lang.ir/en> module for interfacing with L<API Sports Football|https://v3.football.api-sports.io>.

An API key is required to use the API.
=end DESCRIPTION

=begin SYNOPSIS
=begin code :lang<raku>
use APISports::Football:auth<zef:CIAvash>;

my $f = APISports::Football.new: :api_key<1234>;

my @competitions = $f.competitions: name => 'premier league', :current, type => LeagueType::League, :country<england>;
=end code
=end SYNOPSIS

=begin INSTALLATION
You need to have L<Raku|https://www.raku-lang.ir/en> and L<zef|https://github.com/ugexe/zef>, then run:

=begin code :lang<console>
zef install "APISports::Football:auth<zef:CIAvash>"
=end code

or if you have cloned the repo:

=begin code :lang<console>
zef install .
=end code
=end INSTALLATION

=begin TESTING
=begin code :lang<console>
prove -ve 'raku -I.' --ext rakutest
=end code
or
=begin code :lang<console>
prove6 -I. -v
=end code
=end TESTING

use v6.d;

use APISports::Football::HTTPClient:auth($?DISTRIBUTION.meta<auth>);
use APISports::Football::Types:auth($?DISTRIBUTION.meta<auth>) :subsets, :types;
use APISports::Football::Endpoints:auth($?DISTRIBUTION.meta<auth>);

sub EXPORT {
    Map.new: 'LeagueType'    => LeagueType,
             'MatchStats'    => MatchStats,
             'MatchStatus'   => MatchStatus,
             'FixtureEvent'  => FixtureEvent,
             'FixtureLineup' => FixtureLineup,
}

unit class APISports::Football:auth($?DISTRIBUTION.meta<auth>):ver($?DISTRIBUTION.meta<version>);

=ATTRIBUTES

#| API key required by api-football.com
has Str $.api_key is required;

#| An object for making requests to api-football.com
has APISports::Football::HTTPClient $.http_client .= new: :$!api_key;

=METHODS

#| Get the respone object containing list of available timezones.
method timezones (Bool :h(:$http_body) #=[ Get the HTTP response body ]) {
    $!http_client.get: Endpoints::Timezones, :$http_body
}

#| Get the respone object containing list of available countries.
method countries (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        Str        :$name,  #= Name of the country
        TwoChars   :$code,  #= Code of the country
        SearchTerm :$search #= Name of the country
    )
) {
    $!http_client.get: Endpoints::Countries, :$http_body, :%params
}

#| Get the respone object containing list of available seasons for competitions.
method seasons (Bool :h(:$http_body) #=[ Get the HTTP response body ]) {
    $!http_client.get: Endpoints::Seasons, :$http_body
}

#| Get the respone object containing list of available competitions.
method competitions (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID              :$id,      #= ID of the league
        Str             :$name,    #= Name of the league
        Str             :$country, #= Country name of the league
        TwoChars        :$code,    #= Code of the country
        Year            :$season,  #= Season of the league
        ID              :$team,    #= ID of the team
        LeagueType(Str) :$type,    #= Type of the league
        Bool            :$current, #= State of the league
        SearchTerm      :$search,  #= Name or country name of the league
        AtMost2Digits   :$last     #= The X last leagues added to the API
    )
) {
    $!http_client.get: Endpoints::Competitions, :$http_body, :%params
}

#| Get the respone object containing list of available clubs.
method clubs (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID         :$id,      #= ID of the team
        Str        :$name,    #= Name of the team
        ID         :$league,  #= ID of the league
        Str        :$country, #= Country name of the team
        Year       :$season,  #= Season of the league
        SearchTerm :$search   #= Name or country name of the team
    )
) {
    $!http_client.get: Endpoints::Clubs, :$http_body, :%params
}

#| Get the respone object containing team's statistics.
method team_stats (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID     :$team!,   #= ID of the team
        ID     :$league!, #= ID of the league
        Year   :$season!, #= Season of the league
        Date() :$date     #= The limit date
    )
) {
    $!http_client.get: Endpoints::TeamStats, :$http_body, :%params
}

#| Get the respone object containing list of available seasons for a team.
method team_seasons (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID :$team! #= ID of the team
    )
) {
    $!http_client.get: Endpoints::TeamSeasons, :$http_body, :%params
}

#| Get the respone object containing list of available venues.
method venues (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID         :$id,      #= ID of the venue
        Str        :$name,    #= Name of the venue
        Str        :$country, #= Country name of the venue
        SearchTerm :$search   #= Name, city or the country of the venue
    )
) {
    $!http_client.get: Endpoints::Venues, :$http_body, :%params
}

#| Get the respone object containing standings of a league or team.
method tables (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID   :$team,   #= ID of the team
        ID   :$league, #= ID of the league
        Year :$season! #= Season of the league
    )
) {
    $!http_client.get: Endpoints::Tables, :$http_body, :%params
}

#| Get the respone object containing list of rounds for a league or cup.
method rounds (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID   :$league!, #= ID of the league
        Year :$season!, #= Season of the league
        Bool :$current  #= Current round only
    )
) {
    $!http_client.get: Endpoints::Rounds, :$http_body, :%params
}

#| Get the respone object containing list of available matches.
method matches (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID               :$id,      #= ID of the fixture
        AllOrIDs         :$live,    #= all or IDs of two teams
        Date()           :$date,    #= A valid date
        ID               :$league,  #= ID of the league
        Year             :$season,  #= Season of the league
        ID               :$team,    #= ID of the team
        AtMost2Digits    :$last,    #= For the X last fixtures
        AtMost2Digits    :$next,    #= For the X next fixtures
        Date()           :$from,    #= Fixtures from date
        Date()           :$to,      #= Fixtures to date
        Str              :$round,   #= Round of the fixture
        MatchStatus(Str) :$status,  #= Status of the fixture
        Timezone         :$timezone #= A valid timezone
    )
) {
    $!http_client.get: Endpoints::Matches, :$http_body, :%params
}

#| Get the respone object containing list of head to heads between two teams.
method head_to_head (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        IDs              :$h2h!,    #= IDs of two teams
        Date()           :$date,    #= Date of the fixtures
        ID               :$league,  #= ID of the league
        Year             :$season,  #= Season of the league
        AtMost2Digits    :$last,    #= For the X last fixtures
        AtMost2Digits    :$next,    #= For the X next fixtures
        Date()           :$from,    #= Fixtures from date
        Date()           :$to,      #= Fixtures to date
        MatchStatus(Str) :$status,  #= Status of the fixture
        Timezone         :$timezone #= A valid timezone
    )
) {
    $!http_client.get: Endpoints::HeadToHead, :$http_body, :%params
}

#| Get the respone object containing statistics of a match.
method match_stats (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID              :$fixture!, #= ID of the fixture
        ID              :$team,     #= ID of the team
        MatchStats(Str) :$type      #= Type of statistics
    )
) {
    $!http_client.get: Endpoints::MatchStats, :$http_body, :%params
}

#| Get the respone object containing events for a fixture.
method fixture_events (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID                :$fixture!, #= ID of the fixture
        ID                :$team,     #= ID of the team
        ID                :$player,   #= ID of the player
        FixtureEvent(Str) :$type      #= Type of events
    )
) {
    $!http_client.get: Endpoints::FixtureEvents, :$http_body, :%params
}

#| Get the respone object containing lineups for a fixture.
method fixture_lineups (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID                 :$fixture!, #= ID of the fixture
        ID                 :$team,     #= ID of the team
        ID                 :$player,   #= ID of the player
        FixtureLineup(Str) :$type      #= Type of lineups
    )
) {
    $!http_client.get: Endpoints::FixtureLineups, :$http_body, :%params
}

#| Get the respone object containing statistics for players from a fixture.
method fixture_players (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID :$fixture!, #= ID of the fixture
        ID :$team      #= ID of the team
    )
) {
    $!http_client.get: Endpoints::FixturePlayers, :$http_body, :%params
}

#| Get the respone object containing list of players not participating in matches.
method injuries (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID       :$league,  #= ID of the league
        ID       :$fixture, #= ID of the fixture
        ID       :$team,    #= ID of the team
        ID       :$player,  #= ID of the player
        Date()   :$date,    #= Date of the injury
        Year     :$season,  #= Season of the league, required with league, team and player parameters
        Timezone :$timezone #= A valid timezone
    )
) {
    $!http_client.get: Endpoints::Injuries, :$http_body, :%params
}

#| Get the respone object containing predictions for a fixture.
method predictions (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID :$fixture! #= ID of the fixture
    )
) {
    $!http_client.get: Endpoints::Predictions, :$http_body, :%params
}

#| Get the respone object containing information about coachs and their careers.
method coachs (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID         :$id,    #= ID of the coach
        ID         :$team,  #= ID of the team
        SearchTerm :$search #= Name of the coach
    )
) {
    $!http_client.get: Endpoints::Coachs, :$http_body, :%params
}

#| Get the respone object containing information and statistics about players.
method players (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID         :$id,     #= ID of the player
        ID         :$team,   #= ID of the team
        ID         :$league, #= ID of the league
        Year       :$season, #= Season of the league
        SearchTerm :$search, #= Name of the player
        Int        :$page    #= Use for pagination
    )
) {
    $!http_client.get: Endpoints::Players, :$http_body, :%params
}

#| Get the respone object containing list of available seasons for players statistics.
method player_seasons (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID :$player #= ID of the player
    )
) {
    $!http_client.get: Endpoints::PlayerSeasons, :$http_body, :%params
}

#| Get the respone object containing squad of a team or squads of a player.
method squads (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID :$player, #= ID of the player
        ID :$team    #= ID of the team
    )) {
    $!http_client.get: Endpoints::Squads, :$http_body, :%params
}

#| Get the respone object containing best players for a league or cup.
method top_scorers (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID   :$league!, #= ID of the league
        Year :$season!  #= Season of the league
    )
) {
    $!http_client.get: Endpoints::TopScorers, :$http_body, :%params
}

#| Get the respone object containing best assists for a league or cup.
method top_assists (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID   :$league!, #= ID of the league
        Year :$season!  #= Season of the league
    )
) {
    $!http_client.get: Endpoints::TopAssists, :$http_body, :%params
}

#| Get the respone object containing players with the most yellow cards for a league or cup.
method top_yellow_cards (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID   :$league!, #= ID of the league
        Year :$season!  #= Season of the league
    )
) {
    $!http_client.get: Endpoints::TopYellowCards, :$http_body, :%params
}

#| Get the respone object containing players with the most red cards for a league or cup.
method top_red_cards (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID   :$league!, #= ID of the league
        Year :$season!  #= Season of the league
    )
) {
    $!http_client.get: Endpoints::TopRedCards, :$http_body, :%params
}

#| Get the respone object containing all available transfers for players and teams.
method transfers (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID :$player, #= ID of the player
        ID :$team    #= ID of the team
    )
) {
    $!http_client.get: Endpoints::Transfers, :$http_body, :%params
}

#| Get the respone object containing all available trophies for a player or coach.
method trophies (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID :$player, #= ID of the player
        ID :$coach   #= ID of the coach
    )
) {
    $!http_client.get: Endpoints::Trophies, :$http_body, :%params
}

#| Get the respone object containing all available sidelined information for a player or coach.
method sidelined (
    Bool :h(:$http_body), #= Get the HTTP response body
    *%params (
        ID :$player, #= ID of the player
        ID :$coach   #= ID of the coach
    )
) {
    $!http_client.get: Endpoints::Sidelined, :$http_body, :%params
}

=ERRORS
=item If a response contains errors, a C<Failure> of type C<X::APISports::Football::APIError> is returned.
=item If HTTP errors occur, C<Failure> of type C<X::APISports::Football::HTTPError> is returned.

=REPOSITORY L<https://codeberg.org/CIAvash/APISports-Football>

=BUGS L<https://codeberg.org/CIAvash/APISports-Football/issues>

=AUTHOR Siavash Askari Nasr L<https://siavash.askari-nasr.com>

=COPYRIGHT Copyright © 2022 Siavash Askari Nasr

=begin LICENSE
This file is part of APISports::Football.

APISports::Football is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

APISports::Football is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with APISports::Football.  If not, see <http://www.gnu.org/licenses/>.
=end LICENSE
